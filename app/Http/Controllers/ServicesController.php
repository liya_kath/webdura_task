<?php

namespace App\Http\Controllers;
use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
public function index($id)
{
    $service=Services::all(); 
    
    return view("new",["service"=>$service,"id"=>$id]);
}
public function getRequests()
{
    
    
    $id=$_POST['service_id'];

    $start=$_POST['start'];
    $status=$_POST['status'];
   
    $count=5;
    $where="";
    if($id!="")
    {
        $where.='WHERE service_id='.$id.'';
    }
    if($status!="")
    {
        $where.='&& status='.$status.'';
    }
   

    $users = DB::select('SELECT * FROM booking b left join users u on(b.user_id=u.id) left join services s on(s.id=b.service_id)  '.$where.' order by book_id desc limit '.$start.','.$count.' ');
    echo json_encode($users);exit;
}
public function updateStatus()
{
    $id=$_POST['id'];
    $status=$_POST['status'];

    $data= DB::table('booking')
        ->where('book_id',$id)
        ->update(array(
                  'status'=>$status,
                  
         ));
         $result=array();
         if($data)
         {
             
             $result['result']="success";
         }
         else
         {
            $result['result']="fail";

         }
         echo json_encode($result);exit;
}

}

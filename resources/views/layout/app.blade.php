
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
            .MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 100%; position:relative; }
    .MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
        .MultiCarousel .MultiCarousel-inner .item { float: left;}
        .MultiCarousel .MultiCarousel-inner .item > div { text-align: center; padding:10px; margin:10px; background:#f1f1f1; color:#666;}
    .MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
    .MultiCarousel .leftLst { left:0; }
    .MultiCarousel .rightLst { right:0; }
    
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { pointer-events: none; background:#ccc; }
        .container {
  width: 100%;
}

.progressbar {
  counter-reset: step;
}
.progressbar li {
  list-style: none;
  display: inline-block;
  width: 30.33%;
  position: relative;
  text-align: center;
  cursor: pointer;
}
.progressbar li:before {
  content: counter(step);
  counter-increment: step;
  width: 30px;
  height: 30px;
  line-height : 30px;
  border: 1px solid #ddd;
  border-radius: 100%;
  display: block;
  text-align: center;
  margin: 0 auto 10px auto;
  background-color: #fff;
}
.progressbar li:after {
  content: "";
  position: absolute;
  width: 100%;
  height: 1px;
  background-color: #ddd;
  top: 15px;
  left: -50%;
  z-index : -1;
}
.progressbar li:first-child:after {
  content: none;
}
.progressbar li.active {
  color: #17a2b8;
}
.progressbar li.active:before {
  border-color: #17a2b8;
} 
.progressbar li.active + li:after {
  background-color: #17a2b8;
}

.icon-container {
    border-radius: 7px
}

.stars i {
    margin-right: 2px;
    color: red;
    font-size: 13px
}

.rating-number {
    font-size: 20px;
    font-weight: 700;
    margin-bottom: 2px
}

.number-ratings {
    font-size: 12px
}

.listing-title {
    margin-bottom: -7px
}

.progress-bar {
    background: green
}

.progress {
    height: 16px
}

.tags span {
    margin-right: 9px;
    border: 1px solid #17a2b8;
    padding-right: 9px;
    padding-left: 9px;
    padding-top: 2px;
    padding-bottom: 4px;
    border-radius: 7px;
    background-color:#17a2b8;
    color: #fff
}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
    <div class="container">
	<div class="row">
		<div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
            <div class="MultiCarousel-inner">

   
            @foreach($service as $data )

<div class="item">
    <div class="pad15">
        <p class="lead">{{$data->service_name}}</p>
        <p>{{$data->desc}}</p>
        <p><a href="/services/{{$data->id}}?stats=1"><button type="button" class="btn btn-info">Click</button></a></p>
    </div>
</div>

@endforeach   
            </div>
            <button class="btn btn-primary leftLst"><</button>
            <button class="btn btn-primary rightLst">></button>
        </div>
	</div>
<?php
 
?>
<ul class="nav nav-pills nav-fill">
  <li class="nav-item">
    <a class="nav-link <?php echo isset($_GET['stats']) && $_GET['stats']==1? "active":""; ?>" aria-current="page" href="/services/<?php echo $id;?>?stats=1">Request</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php echo isset($_GET['stats']) && $_GET['stats']==2? "active":""; ?>" href="/services/<?php echo $id;?>?stats=2">Service</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php echo isset($_GET['stats']) && $_GET['stats']==3? "active":""; ?>" href="/services/<?php echo $id;?>?stats=3">Payment</a>
  </li>
 
</ul>

<br>
<br>
<div id="content_div"></div>


        <!-- <div onMouseclick="getData()" style="border-color:red ;text-align:center" id="view_more"><b>View More</b></div> -->
        <button onclick="getData(<?php echo $id? $id: '' ?>,<?php echo $_GET['stats']!=''? $_GET['stats']: ''; ?> )" id="view_more" class="btn btn-secondary" type="button"> click here to view more</button>
    </body>
</html>
<script>
    $(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});

setTimeout(function(){ getData(<?php echo $id!=""? $id: "" ?>,<?php echo $_GET['stats']!=""? $_GET['stats']: ""; ?>); }, 500);

function getData(id,status)
{
   

    var start=$(".itemss").length;
    var formData = new FormData();
    formData.append('start', start);
    formData.append('service_id', id);
    formData.append('status', status);
    var html="";
    $.ajax({
                        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url: '/requests', //the url where we want to POST
                        data: formData, // our data object
                        cache: false,
                        dataType: 'json', // what type of data do we expect back from the server
                        processData: false,
                        contentType: false,
                        encode: true

                    })
                    .done(function(data) {

                       
if(typeof data !== 'undefined' && data.length > 0)
{
    let i=1;
    $.each(data, function (index,value) {

        var request="";
        var service="";
        var payment="";

        if(value.status==1)
        {
            var request="active";
            

        }
        else if(value.status==2)
        {
            var request="active";
            var service="active";
            

        }
        else if(value.status==3)
        {
            var request="active";
            var service="active";
            var payment="active";
            

        }

html+=`
<div class="container itemss">
<ul class="progressbar">
<li class=${request}>PENDING</li>
<li class=${service}>ACTIVE</li>
<li class=${payment}>PAYMENT</li>
</ul>
</div>
<div class="col-md-12">
<div class="bg-white p-3 rounded mt-2">
<div class="row">
<div class="col-md-3">
<div class="d-flex flex-column justify-content-center align-items-center icon-container bg-success text-white mb-2"><i class="fa fa-flask fa-5x mb-3 mt-5"></i><span class="mb-4">PHOTO</span></div>
</div>
<div class="col-md-6 border-right">
<div class="listing-title">
<h5>${value.name}</h5>
</div>


<div class="d-flex flex-row align-items-center">
<div class="d-flex flex-row align-items-center ratings"><span class="mr-1 rating-number">${value.service_name}</span>
</div>
</div>
<div class="description">
<p>Customer available</p>
<p>${value.available_date_from}</p>
<p>${value.available_date_to}</p>
</div>
<div class="tags mb-2">
</div>`;
if(value.status==1)
{
    html+=`<button type="button" onClick="updateStatus(${value.book_id},${2})" class="btn btn-primary">Accept Request</button>`;

}
else if(value.status==2)
{
    html+=`<button type="button" onClick="updateStatus(${value.book_id},${3})" class="btn btn-primary">Get Invoice</button>`;

}else if(value.status==3)
{
    html+=`<button type="button" disabled class="btn btn-primary">Payment</button>`;

}

html+=`</div>

</div>
</div>
</div>`;

i++;

});
}
else
{
    html+=`
<div class="col-md-12">
<div class="bg-white p-3 rounded mt-2">
<div class="row" style="text-align:center">

<br>
<br>
<b>No More booking found</b>

</div>
</div>
</div>`;

$("#view_more").hide(); 
}

$('#content_div').append(html);      
       })
                    .fail(function(data) {

                        
                    });
 

     
}
    </script>
    <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function updateStatus(id,status)
{
   
    var formData = new FormData();
    formData.append('id', id);
    formData.append('status',status);
    var html="";
    $.ajax({
                        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url: '/update_status', //the url where we want to POST
                        data: formData, // our data object
                        cache: false,
                        dataType: 'json', // what type of data do we expect back from the server
                        processData: false,
                        contentType: false,
                        encode: true

                    })
                    .done(function(data) {
 
                      
                        alert(data.result);
                        
                        window.location.href = "/services/<?php echo $id!=""? $id: "" ?>?stats=<?php echo $_GET['stats']!=""? $_GET['stats']: ""; ?>";

                    });

                    
}
</script>

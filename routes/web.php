<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/services/1?stats=1');
});
Route::get('/services', function () {
    return redirect('/services/1?stats=1');
});



Route::get('/services/{id}', 'App\Http\Controllers\ServicesController@index');
Route::post('/requests', 'App\Http\Controllers\ServicesController@getRequests');
Route::get('/requests', 'App\Http\Controllers\ServicesController@updateStatus');
Route::post('/update_status', 'App\Http\Controllers\ServicesController@updateStatus');


